/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/RootList/Assembly/Namespace.h>
#include <GitComplex/Repository/RootList/Interactor/Detail/Editor.h>
#include <GitComplex/Repository/RootList/Presenter/Actions/Detail/Editor.h>
#include <GitComplex/UserInterface/Action/Provider.h>
#include <GitComplex/Settings/Storable.h>

#include <GitComplex/Assembly/Export.h>

/*!
 * \file include/GitComplex/Repository/RootList/Assembly/Editor.h
 * \brief include/GitComplex/Repository/RootList/Assembly/Editor.h
 */

namespace GitComplex::Repository::RootList::Assembly
{

/*!
 * \class Editor
 * \brief Editor.
 *
 * \headerfile GitComplex/Repository/RootList/Assembly/Editor.h
 *
 * Editor.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::Assembly]] [[Framework::Qt]]
 */
class GITCOMPLEX_ASSEMBLY_EXPORT
        Editor final : public Actions::Provider<Actions::Editor>,
                       public Settings::RepositoryStorable
{
public:
    Editor(otn::weak_optional<Observer> observer,
           otn::slim::unique_single<Entity::Provider> add_provider,
           otn::slim::unique_single<Entity::Provider> remove_provider);

    Actions::Editor actions(Actions::Tag<Actions::Editor> = {}) const override
    { return m_actions_detail.actions(); }

    void save(Settings::RepositoryStorage& storage) const override;
    void load(Settings::RepositoryStorage& storage) override;

private:
    using Detail = Interactor::Detail::Editor;

    otn::slim::unique_single<Entity::Provider> m_add_provider;
    otn::slim::unique_single<Entity::Provider> m_remove_provider;
    otn::unique_single<Detail> m_detail;
    Actions::Detail::Editor    m_actions_detail;
};

} // namespace GitComplex::Repository::RootList::Assembly
