/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Tree/Assembly/Namespace.h>
#include <GitComplex/Repository/Tree/Presenter/Model.h>
#include <GitComplex/UserInterface/Widget/Ownership.h>

#include <QTreeView>

#include <otn/all.hpp>
#include <GitComplex/Integration/Otl/Qt.h>

/*!
 * \file include/GitComplex/Repository/Tree/Assembly/Storage.h
 * \brief include/GitComplex/Repository/Tree/Assembly/Storage.h
 */

namespace GitComplex::Repository::Tree::Assembly
{

/*!
 * \class Storage
 * \brief Storage.
 *
 * \headerfile GitComplex/Repository/Tree/Assembly/Storage.h
 *
 * Storage.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::Assembly]] [[Framework::Qt]]
 */
class Storage final
{
public:
    Storage();

    operator const Observer&() const noexcept
    { return *m_storage; }
    operator otn::weak_optional<Observer>() const noexcept
    { return m_storage; }
    operator otn::weak_optional<const DependenceProvider>() const noexcept
    { return m_storage; }
    operator otn::weak_optional<const Presenter::InfoProvider>() const noexcept
    { return m_storage; }

    operator QTreeView&() const noexcept
    { return *m_observer_view; }

    View::Widget::Reference<QTreeView> observerView() const
    { return m_observer_view; }

private:
    otn::unique_single<Presenter::Model> m_storage;
    View::Widget::Owner<QTreeView>       m_observer_view;
};

} // namespace GitComplex::Repository::Tree::Assembly
