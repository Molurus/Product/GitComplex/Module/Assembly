/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Synchronizer/Assembly/TreeContent.h>
#include <GitComplex/Repository/Synchronizer/Interactor/Concrete/TreeStructure.h>
#include <GitComplex/Repository/Synchronizer/Interactor/Concrete/TreeStatus.h>
#include <GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStructure.h>
#include <GitComplex/Repository/Synchronizer/Presenter/Actions/Detail/TreeStatus.h>

/*!
 * \file include/GitComplex/Repository/Synchronizer/Assembly/Concrete/TreeContent.h
 * \brief include/GitComplex/Repository/Synchronizer/Assembly/Concrete/TreeContent.h
 */

namespace GitComplex::Repository::Synchronizer::Assembly::Concrete
{

/*!
 * \class TreeContent
 * \brief TreeContent.
 *
 * \headerfile GitComplex/Repository/Synchronizer/Assembly/Concrete/TreeContent.h
 *
 * TreeContent.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Assembly]] [[Framework::Qt]]
 */
class TreeContent final : public Assembly::TreeContent
{
public:
    using StructureScanner = Scanner::FileSystem;

    TreeContent(otn::weak_optional<Tree::Observer>            tree_observer,
                otn::raw::weak_single<const IdProvider>       id_provider,
                otn::raw::weak_single<const StructureScanner> structure_scanner,
                const RootList::Observable& root_list_observable,
                otn::weak_optional<const RootList::Provider>       root_list_provider,
                otn::raw::weak_single<const Tree::StatusProvider>  tree_status_provider,
                otn::weak_optional<const Tree::DependenceProvider> tree_dependence_provider,
                const Tree::StructureObservable& tree_structure_observable,
                otn::unique_single<const Selection::Provider> selection_provider)
        : m_structure{otn::itself,
                      tree_observer,
                      std::move(id_provider),
                      std::move(structure_scanner),
                      root_list_observable,
                      std::move(root_list_provider)},
          m_status{otn::itself,
                   std::move(tree_observer),
                   std::move(tree_status_provider),
                   std::move(tree_dependence_provider),
                   tree_structure_observable,
                   std::move(selection_provider)},
          m_structure_actions_detail{otn::conform::weak(m_structure)},
          m_status_actions_detail{otn::conform::weak(m_status)}
    {}

private:
    TreeStructureActions actions(Actions::Tag<TreeStructureActions>) const override
    { return m_structure_actions_detail.actions(); }

    TreeStatusActions actions(Actions::Tag<TreeStatusActions>) const override
    { return m_status_actions_detail.actions(); }

    otn::unique_single<Interactor::Concrete::TreeStructure> m_structure;
    otn::unique_single<Interactor::Concrete::TreeStatus>    m_status;
    Actions::Detail::TreeStructure m_structure_actions_detail;
    Actions::Detail::TreeStatus    m_status_actions_detail;
};

} // namespace GitComplex::Repository::Synchronizer::Assembly::Concrete
