/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Entity/Qt/IdProvider.h>
#include <GitComplex/Repository/RootList/Entity/Storage.h>
#include <GitComplex/Repository/Tree/Entity/Qt/StatusProvider.h>
#include <GitComplex/Repository/Tree/Assembly/Storage.h>
#include <GitComplex/Repository/Tree/Assembly/ToolLauncher.h>
#include <GitComplex/Repository/Selection/Entity/Provider.h>
#include <GitComplex/Repository/Scanner/Entity/Qt/FileSystem.h>
#include <GitComplex/Repository/Synchronizer/Assembly/TreeContent.h>
#include <GitComplex/BatchWorkspace/View/ObserverPlaceholder.h>
#include <GitComplex/UserInterface/Dialog/Confirmation.h>

/*!
 * \file include/GitComplex/BatchWorkspace/Assembly/RepositoryObserver.h
 * \brief include/GitComplex/BatchWorkspace/Assembly/RepositoryObserver.h
 */

namespace GitComplex::BatchWorkspace::Assembly
{

/*!
 * \class RepositoryObserver
 * \brief RepositoryObserver.
 *
 * \headerfile GitComplex/BatchWorkspace/Assembly/RepositoryObserver.h
 *
 * RepositoryObserver.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::Assembly]] [[Framework::Qt]]
 */
class RepositoryObserver final
{
public:
    using IdProvider         = Repository::Entity::Qt::IdProvider;
    using StructureScanner   = Repository::Scanner::Entity::Qt::FileSystem;
    using RootListObservable = Repository::RootList::Entity::Observable;
    using RootListProvider   = Repository::RootList::Entity::Provider;
    using StatusProvider     = Repository::Tree::Entity::Qt::StatusProvider;
    using DependenceProvider = Repository::Tree::Entity::Qt::DependenceProvider;
    using ToolLauncher       = Repository::Shell::Entity::Qt::ToolLauncher;

    RepositoryObserver(otn::raw::weak_single<const IdProvider>       id_provider,
                       otn::raw::weak_single<const StructureScanner> structure_scanner,
                       const RootListObservable& root_list_observable,
                       otn::weak_optional<const RootListProvider>  root_list_provider,
                       otn::raw::weak_single<const StatusProvider> status_provider,
                       otn::raw::weak_single<const ToolLauncher>   tool_launcher);

    using SelectionProvider = Repository::Selection::Entity::Provider;
    using Confirmation      = UserInterface::Dialog::Confirmation;

    otn::slim::unique_single<SelectionProvider> makeIdSelector();
    otn::slim::unique_single<RootListProvider>  makePathSelector(
        Confirmation confirmation = Confirmation::None);

    View::Widget::Reference<View::ObserverPlaceholder> view()
    { return m_view; }

private:
    using TreeStorage      = Repository::Tree::Assembly::Storage;
    using TreeSynchronizer = Repository::Synchronizer::Assembly::TreeContent;
    using TreeToolLauncher = Repository::Tree::Assembly::ToolLauncher;

    TreeStorage      m_tree_storage;
    otn::slim::unique_single<TreeSynchronizer> m_tree_synchronizer;
    TreeToolLauncher m_tree_tool_launcher;

    View::Widget::Owner<View::ObserverPlaceholder> m_view;
};

} // namespace GitComplex::BatchWorkspace::Assembly
