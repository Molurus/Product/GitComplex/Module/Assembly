/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/BatchWorkspace/Assembly/Namespace.h>
#include <GitComplex/BatchWorkspace/Presenter/Actions/Detail/BatchConsole.h>
#include <GitComplex/BatchWorkspace/View/ConsolePlaceholder.h>
#include <GitComplex/Repository/Selection/Entity/Provider.h>
#include <GitComplex/Repository/Shell/Entity/Qt/CommandExecutor.h>
#include <GitComplex/Settings/Storage.h>

#include <GitComplex/Integration/Otl/Qt.h>

/*!
 * \file include/GitComplex/BatchWorkspace/Assembly/BatchConsole.h
 * \brief include/GitComplex/BatchWorkspace/Assembly/BatchConsole.h
 */

namespace GitComplex::BatchWorkspace::Assembly
{

/*!
 * \class BatchConsole
 * \brief BatchConsole.
 *
 * \headerfile GitComplex/BatchWorkspace/Assembly/BatchConsole.h
 *
 * BatchConsole.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::Assembly]] [[Framework::Qt]]
 */
class BatchConsole final
{
public:
    using CommandExecutor   = const Repository::Shell::Entity::Qt::CommandExecutor;
    using SelectionProvider = const Repository::Selection::Entity::Provider;

    BatchConsole(otn::raw::weak_single<CommandExecutor> command_executor,
                 otn::unique_single<SelectionProvider>  selection_provider)
        : BatchConsole{build(std::move(command_executor),
                             std::move(selection_provider))}
    {}

    void save(Settings::RepositoryStorage& storage) const;
    void load(Settings::RepositoryStorage& storage);

    View::Widget::Reference<View::ConsolePlaceholder> view() const
    { return m_view; }

private:
    BatchConsole(otn::unique_single<Interactor::BatchConsole> console,
                 Actions::Detail::BatchConsole actions_detail,
                 View::Widget::Owner<View::ConsolePlaceholder> view)
        : m_console{std::move(console)},
          m_actions_detail{std::move(actions_detail)},
          m_view{std::move(view)}
    {}

    static BatchConsole build(
        otn::raw::weak_single<CommandExecutor> command_executor,
        otn::unique_single<SelectionProvider>  selection_provider);

    otn::unique_single<Interactor::BatchConsole> m_console;
    Actions::Detail::BatchConsole m_actions_detail;

    View::Widget::Owner<View::ConsolePlaceholder> m_view;
};

} // namespace GitComplex::BatchWorkspace::Assembly
