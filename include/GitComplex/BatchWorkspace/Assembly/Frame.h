/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/BatchWorkspace/Assembly/RepositoryObserver.h>
#include <GitComplex/BatchWorkspace/Assembly/BatchConsole.h>
#include <GitComplex/BatchWorkspace/View/Frame.h>
#include <GitComplex/UserInterface/Dialog/Confirmation.h>

#include <GitComplex/Assembly/Export.h>

/*!
 * \file include/GitComplex/BatchWorkspace/Assembly/Frame.h
 * \brief include/GitComplex/BatchWorkspace/Assembly/Frame.h
 */

namespace GitComplex::BatchWorkspace::Assembly
{

/*!
 * \class Frame
 * \brief Frame.
 *
 * \headerfile GitComplex/BatchWorkspace/Assembly/Frame.h
 *
 * Frame.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::Assembly]] [[Framework::Qt]]
 */
class GITCOMPLEX_ASSEMBLY_EXPORT
        Frame final
{
public:
    using RepositoryObserver = BatchWorkspace::Assembly::RepositoryObserver;
    using BatchConsole       = BatchWorkspace::Assembly::BatchConsole;

    Frame(otn::raw::weak_single<const RepositoryObserver::IdProvider>       id_provider,
          otn::raw::weak_single<const RepositoryObserver::StructureScanner> structure_scanner,
          const RepositoryObserver::RootListObservable& root_list_observable,
          otn::weak_optional<const RepositoryObserver::RootListProvider>  root_list_provider,
          otn::raw::weak_single<const RepositoryObserver::StatusProvider> status_provider,
          otn::raw::weak_single<const RepositoryObserver::ToolLauncher>   tool_launcher,
          otn::raw::weak_single<const BatchConsole::CommandExecutor>      command_executor);

    void save(Settings::RepositoryStorage& storage) const;
    void load(Settings::RepositoryStorage& storage);

    using RootListProvider  = Repository::RootList::Entity::Provider;
    using SelectionProvider = Repository::Selection::Entity::Provider;
    using Confirmation      = UserInterface::Dialog::Confirmation;

    otn::slim::unique_single<SelectionProvider> makeIdSelector();
    otn::slim::unique_single<RootListProvider>  makePathSelector(
        Confirmation confirmation = Confirmation::None);

    View::Widget::Reference<View::Frame> view() { return m_view; }

private:
    otn::unique_single<RepositoryObserver> m_repository_observer;
    otn::unique_single<BatchConsole>       m_batch_console;

    View::Widget::Owner<View::Frame>       m_view;
};

} // namespace GitComplex::BatchWorkspace::Assembly
