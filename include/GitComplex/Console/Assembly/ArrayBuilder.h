/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Console/Assembly/Namespace.h>
#include <GitComplex/Console/Entity/Array.h>
#include <GitComplex/Console/View/Detail/StackedSplitter.h>
#include <GitComplex/UserInterface/Widget/Ownership.h>

#include <otn/all.hpp>

/*!
 * \file include/GitComplex/Console/Assembly/ArrayBuilder.h
 * \brief include/GitComplex/Console/Assembly/ArrayBuilder.h
 */

namespace GitComplex::Console::Assembly
{

/*!
 * \class ArrayBuilder
 * \brief ArrayBuilder.
 *
 * \headerfile GitComplex/Console/Assembly/ArrayBuilder.h
 *
 * ArrayBuilder.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::Assembly]] [[Framework::Qt]]
 */
template <std::size_t ConsoleCount>
class ArrayBuilder final
{
public:
    ArrayBuilder() : ArrayBuilder{build()} {}

    otn::unique_single<Array> releaseConsoles()
    { return std::move(m_consoles); }

    View::Widget::Owner<> releaseView()
    { return std::move(m_view); }

private:
    ArrayBuilder(otn::unique_single<Array> consoles,
                 View::Widget::Owner<>     view)
        : m_consoles{std::move(consoles)},
          m_view{std::move(view)}
    {}

    static ArrayBuilder build();

    //
    otn::unique_single<Array> m_consoles;
    View::Widget::Owner<>     m_view;
};

} // namespace GitComplex::Console::Assembly

#include <GitComplex/Console/Entity/Concrete/Array.h>
#include <GitComplex/Console/View/Concrete/Splitted.h>

namespace GitComplex::Console::Assembly
{

namespace
{

void configureEditor(View::Detail::Editor& editor)
{
    QSizePolicy policy = editor.sizePolicy();
    policy.setVerticalStretch(1);
    editor.setSizePolicy(policy);
}

void configureOutput(View::Detail::OutputView& output)
{
    QSizePolicy policy = output.sizePolicy();
    policy.setVerticalStretch(4);
    output.setSizePolicy(policy);
}

} // namespace

template <std::size_t ConsoleCount>
ArrayBuilder<ConsoleCount> ArrayBuilder<ConsoleCount>::build()
{
    // View of the single console.
    using SingleView = View::Concrete::Splitted;
    // View of the array of single consoles.
    using ArrayView = View::Detail::StackedSplitter;

    // Widgets of the single console.
    using SingleWidgets = ArrayView::SplittedArray<2>;
    // Widgets of the array of single consoles.
    using ArrayWidgets = ArrayView::StackedArray<ConsoleCount, 2>;

    using ConcreteConsoles = Concrete::Array<ConsoleCount>;
    using UniqueConsole    = typename ConcreteConsoles::UniqueConsole;
    using Consoles         = typename ConcreteConsoles::Consoles;

    using Editor = View::Detail::Editor;
    using Output = View::Detail::OutputView;

    // All
    auto* editor_all = new Editor();
    auto* output_all = new Output();

    configureEditor(*editor_all);
    configureOutput(*output_all);

    // Selected
    auto* editor_selected = new Editor();
    auto* output_selected = new Output();

    configureEditor(*editor_selected);
    configureOutput(*output_selected);

    // Checked
    auto* editor_checked = new Editor();
    auto* output_checked = new Output();

    configureEditor(*editor_checked);
    configureOutput(*output_checked);

    // -----
    UniqueConsole console_all{
        otn::itself_type<SingleView>, editor_all, output_all};
    UniqueConsole console_selected{
        otn::itself_type<SingleView>, editor_selected, output_selected};
    UniqueConsole console_checked{
        otn::itself_type<SingleView>, editor_checked, output_checked};

    otn::unique_single<Array> consoles{
        otn::itself_type<ConcreteConsoles>,
        Consoles{std::move(console_all),
                 std::move(console_selected),
                 std::move(console_checked)}};

    // -----
    ArrayWidgets widget_array{SingleWidgets{editor_all, output_all},
                              SingleWidgets{editor_selected, output_selected},
                              SingleWidgets{editor_checked, output_checked}};

    View::Widget::Owner<ArrayView> view{otn::itself, std::move(widget_array)};

    return ArrayBuilder{std::move(consoles),
                        std::move(view)};
}

} // namespace GitComplex::Console::Assembly
