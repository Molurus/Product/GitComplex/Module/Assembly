/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/BatchWorkspace/Assembly/Frame.h>
#include <GitComplex/BatchWorkspace/View/Detail/Frame.h>
#include <GitComplex/Settings/Utility.h>

/*!
 * \file src/GitComplex/BatchWorkspace/Assembly/Frame.cpp
 * \brief src/GitComplex/BatchWorkspace/Assembly/Frame.cpp
 */

namespace GitComplex::BatchWorkspace::Assembly
{

Frame::Frame(otn::raw::weak_single<const RepositoryObserver::IdProvider>       id_provider,
             otn::raw::weak_single<const RepositoryObserver::StructureScanner> structure_scanner,
             const RepositoryObserver::RootListObservable& root_list_observable,
             otn::weak_optional<const RepositoryObserver::RootListProvider>  root_list_provider,
             otn::raw::weak_single<const RepositoryObserver::StatusProvider> status_provider,
             otn::raw::weak_single<const RepositoryObserver::ToolLauncher>   tool_launcher,
             otn::raw::weak_single<const BatchConsole::CommandExecutor>      command_executor)
    : m_repository_observer{otn::itself,
                            std::move(id_provider),
                            std::move(structure_scanner),
                            root_list_observable,
                            root_list_provider,
                            std::move(status_provider),
                            std::move(tool_launcher)},
      m_batch_console{otn::itself,
                      std::move(command_executor),
                      (*m_repository_observer).makeIdSelector()},
      m_view{otn::itself_type<View::Detail::Frame>}
{
    (*m_view).emplaceWidget(
        View::Widget::Slot<View::ObserverPlaceholder>{(*m_repository_observer).view()});
    (*m_view).emplaceWidget(
        View::Slot::ConsolePlaceholder{(*m_batch_console).view()});
}

void Frame::save(Settings::RepositoryStorage& storage) const
{
    Settings::save_cnt(*m_batch_console, storage, "Console");
    Settings::save_pmr(*m_view, storage, "View");
}

void Frame::load(Settings::RepositoryStorage& storage)
{
    Settings::load_cnt(*m_batch_console, storage, "Console");
    Settings::load_pmr(*m_view, storage, "View");
}

otn::slim::unique_single<Frame::SelectionProvider> Frame::makeIdSelector()
{
    return (*m_repository_observer).makeIdSelector();
}

otn::slim::unique_single<Frame::RootListProvider>
Frame::makePathSelector(Confirmation confirmation)
{
    return (*m_repository_observer).makePathSelector(confirmation);
}

} // namespace GitComplex::BatchWorkspace::Assembly
