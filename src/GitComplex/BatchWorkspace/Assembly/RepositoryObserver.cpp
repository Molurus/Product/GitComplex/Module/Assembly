/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/BatchWorkspace/Assembly/RepositoryObserver.h>
#include <GitComplex/BatchWorkspace/View/Detail/ObserverPlaceholder.h>
#include <GitComplex/Repository/Selector/View/Concrete/TreeViewIdSelector.h>
#include <GitComplex/Repository/Selector/View/Concrete/TreeViewPathSelector.h>
#include <GitComplex/Repository/Synchronizer/Assembly/Concrete/TreeContent.h>

/*!
 * \file src/GitComplex/BatchWorkspace/Assembly/RepositoryObserver.cpp
 * \brief src/GitComplex/BatchWorkspace/Assembly/RepositoryObserver.cpp
 */

namespace GitComplex::BatchWorkspace::Assembly
{

using ItemViewPointer         = QPointer<const QAbstractItemView>;
using ConcreteTreePlaceholder =
    BatchWorkspace::View::Detail::ObserverPlaceholder;
using ConcreteTreeSynchronizer =
    Repository::Synchronizer::Assembly::Concrete::TreeContent;

RepositoryObserver::RepositoryObserver(otn::raw::weak_single<const IdProvider>       id_provider,
                                       otn::raw::weak_single<const StructureScanner> structure_scanner,
                                       const RootListObservable& root_list_observable,
                                       otn::weak_optional<const RootListProvider>  root_list_provider,
                                       otn::raw::weak_single<const StatusProvider> tree_status_provider,
                                       otn::raw::weak_single<const ToolLauncher>   tool_launcher)
    : m_tree_synchronizer{otn::itself_type<ConcreteTreeSynchronizer>,
                          m_tree_storage,
                          std::move(id_provider),
                          std::move(structure_scanner),
                          root_list_observable,
                          std::move(root_list_provider),
                          std::move(tree_status_provider),
                          m_tree_storage,
                          m_tree_storage,
                          makeIdSelector()},
      m_tree_tool_launcher{std::move(tool_launcher),
                           m_tree_storage,
                           m_tree_storage},
      m_view{otn::itself_type<ConcreteTreePlaceholder>}
{
    (*m_view).emplaceWidget(
        View::Slot::GenericObserver{m_tree_storage.observerView()});

    using UserInterface::Action::Tag;
    (*m_view).emplaceActions((*m_tree_synchronizer).actions(
                                 Tag<TreeSynchronizer::TreeStructureActions>{}));
    (*m_view).emplaceActions((*m_tree_synchronizer).actions(
                                 Tag<TreeSynchronizer::TreeStatusActions>{}));
}

otn::slim::unique_single<RepositoryObserver::SelectionProvider>
RepositoryObserver::makeIdSelector()
{
    using Repository::Selector::View::Concrete::TreeViewIdSelector;
    return otn::slim::unique_single<TreeViewIdSelector>(
        otn::itself,
        ItemViewPointer{m_tree_storage.observerView()},
        m_tree_storage);
}

otn::slim::unique_single<RepositoryObserver::RootListProvider>
RepositoryObserver::makePathSelector(Confirmation confirmation)
{
    using Repository::Selector::View::Concrete::TreeViewPathSelector;
    return otn::slim::unique_single<TreeViewPathSelector>(
        otn::itself,
        ItemViewPointer{m_tree_storage.observerView()},
        m_tree_storage,
        confirmation);
}

} // namespace GitComplex::BatchWorkspace::Assembly
