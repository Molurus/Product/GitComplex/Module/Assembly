/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/BatchWorkspace/Assembly/BatchConsole.h>
#include <GitComplex/BatchWorkspace/Interactor/Concrete/BatchConsole.h>
#include <GitComplex/BatchWorkspace/View/Detail/ConsolePlaceholder.h>
#include <GitComplex/Console/Assembly/ArrayBuilder.h>
#include <GitComplex/Repository/Selection/Assembly/SwitcherViewBuilder.h>
#include <GitComplex/Repository/Selection/Entity/Concrete/Switcher.h>

/*!
 * \file src/GitComplex/BatchWorkspace/Assembly/BatchConsole.cpp
 * \brief src/GitComplex/BatchWorkspace/Assembly/BatchConsole.cpp
 */

namespace GitComplex::BatchWorkspace::Assembly
{

void BatchConsole::save(Settings::RepositoryStorage& storage) const
{
    Settings::save_pmr(*m_console, storage);
    Settings::save_pmr(*view(),    storage);
}

void BatchConsole::load(Settings::RepositoryStorage& storage)
{
    Settings::load_pmr(*m_console, storage);
    Settings::load_pmr(*view(),    storage);
}

BatchConsole BatchConsole::build(
    otn::raw::weak_single<CommandExecutor> command_executor,
    otn::unique_single<SelectionProvider>  selection_provider)
{
    // Build.
    namespace Selection = Repository::Selection::Entity;
    otn::unique_single<Selection::Switcher>
    selection_switcher{otn::itself_type<Selection::Concrete::Switcher>,
                       Selection::Mode::AllAny};

    Repository::Selection::Assembly::SwitcherViewBuilder
        switcher_view_builder{selection_switcher};

    Console::Assembly::ArrayBuilder<Selection::ModeInfo::Count>
    console_array_builder{};

    otn::unique_single<Interactor::BatchConsole>
    console{otn::itself_type<Interactor::Concrete::BatchConsole>,
            std::move(command_executor),
            console_array_builder.releaseConsoles(),
            std::move(selection_provider),
            std::move(selection_switcher)};

    Actions::Detail::BatchConsole
        actions_detail{otn::conform::weak(console)};

    View::Widget::Owner<View::Detail::ConsolePlaceholder>
    concrete_view{otn::itself};
    View::ConsolePlaceholder& view = *concrete_view;

    auto console_array_view = console_array_builder.releaseView();

    // Connect.
    switcher_view_builder.connectObserver(console_array_view);

    // Emplace a views.
    using namespace BatchWorkspace::View;
    view.emplaceWidget(Slot::ConsoleArray{std::move(console_array_view)});
    view.emplaceWidget(
        Slot::SelectionSwitcher{switcher_view_builder.releaseView()});

    // Emplace an actions.
    view.emplaceActions(actions_detail.actions());

    return BatchConsole{
        std::move(console),
        std::move(actions_detail),
        std::move(concrete_view)
    };
}

} // namespace GitComplex::BatchWorkspace::Assembly
