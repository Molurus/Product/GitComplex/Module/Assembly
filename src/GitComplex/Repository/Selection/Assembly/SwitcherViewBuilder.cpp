/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Selection/Assembly/SwitcherViewBuilder.h>

#include <GitComplex/Integration/Otl/Qt.h>

#include <QComboBox>

/*!
 * \file src/GitComplex/Repository/Selection/Assembly/SwitcherViewBuilder.cpp
 * \brief src/GitComplex/Repository/Selection/Assembly/SwitcherViewBuilder.cpp
 */

namespace GitComplex::Repository::Selection::Assembly
{

SwitcherViewBuilder::SwitcherViewBuilder(otn::weak_optional<Switcher> switcher)
    : m_switcher{std::move(switcher)},
      m_view{otn::itself_type<QComboBox>}
{
    setupView();
    connectSwitcher();
    connectObserver(m_view);
}

void SwitcherViewBuilder::setupView()
{
    auto& view = static_cast<QComboBox&>(*m_view);
    view.addItems(ModeInfo::DisplayName);
    view.setToolTip(QComboBox::tr("Repository Selection Mode"));
}

void SwitcherViewBuilder::connectObserver(QPointer<QObject> observer) const
{
    if (auto switcher = otn::gain(m_switcher))
        QObject::connect(&(*switcher).modeChanged(),
                         &EventDelegate::Switcher::modeChanged,
                         [observer = std::move(observer)](Mode mode)
            {
                if (observer)
                    (*observer).setProperty("currentIndex",
                                            static_cast<int>(mode));
            });
}

void SwitcherViewBuilder::connectSwitcher()
{
    // TODO: Try to avoid multi-step cast.
    auto* combo_box = static_cast<QComboBox*>(static_cast<QWidget*>(m_view));

    QObject::connect(combo_box,
                     QOverload<int>::of(&QComboBox::currentIndexChanged),
                     [switcher = m_switcher](int index)
        {
            otn::access(switcher, [&](auto& switcher)
                        { switcher.switchToMode(static_cast<Mode>(index)); });
        });
}

} // namespace GitComplex::Repository::Selection::Assembly
