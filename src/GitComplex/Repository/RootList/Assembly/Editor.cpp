/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/RootList/Assembly/Editor.h>
#include <GitComplex/Repository/RootList/Assembly/Namespace.h>
#include <GitComplex/Settings/Utility.h>

/*!
 * \file src/GitComplex/Repository/RootList/Assembly/Editor.cpp
 * \brief src/GitComplex/Repository/RootList/Assembly/Editor.cpp
 */

namespace GitComplex::Repository::RootList::Assembly
{

Editor::Editor(otn::weak_optional<Observer> observer,
               otn::slim::unique_single<Entity::Provider> add_provider,
               otn::slim::unique_single<Entity::Provider> remove_provider)
    : m_add_provider{std::move(add_provider)},
      m_remove_provider{std::move(remove_provider)},
      m_detail{otn::itself,
               std::move(observer),
               m_add_provider,
               m_remove_provider},
      m_actions_detail{*m_detail, m_detail}
{}

void Editor::save(Settings::RepositoryStorage& storage) const
{
    Settings::save_pmr(*m_add_provider, storage);
}

void Editor::load(Settings::RepositoryStorage& storage)
{
    Settings::load_pmr(*m_add_provider, storage);
}

} // namespace GitComplex::Repository::RootList::Assembly
