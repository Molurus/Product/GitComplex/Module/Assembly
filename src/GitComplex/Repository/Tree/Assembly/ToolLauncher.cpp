/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Tree/Assembly/ToolLauncher.h>
#include <GitComplex/Repository/Tree/Presenter/Concrete/ToolLauncher.h>

/*!
 * \file src/GitComplex/Repository/Tree/Assembly/ToolLauncher.cpp
 * \brief src/GitComplex/Repository/Tree/Assembly/ToolLauncher.cpp
 */

namespace GitComplex::Repository::Tree::Assembly
{

namespace
{

FixedObjectConnections<1> connect(
    const QAbstractItemView& view,
    const otn::weak_optional<Presenter::ToolLauncher>& presenter)
{
    return {
        QObject::connect(&view, &QAbstractItemView::clicked,
                         [ = ](const QModelIndex& index)
                {
                    otn::access(presenter, [&](auto& presenter)
                                { presenter.launch(index); });
                })
    };
}

} // namespace

ToolLauncher::ToolLauncher(
    otn::raw::weak_single<const ShellToolLauncher>    tool_launcher,
    otn::weak_optional<const Presenter::InfoProvider> info_provider,
    const QAbstractItemView& view)
    : m_presenter{otn::itself_type<Presenter::Concrete::ToolLauncher>,
                  std::move(tool_launcher),
                  std::move(info_provider)},
      m_connections{connect(view, m_presenter)}
{}

} // namespace GitComplex::Repository::Tree::Assembly
