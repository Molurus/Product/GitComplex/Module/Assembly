# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

cmake_minimum_required(VERSION 3.13...3.14.2)

project(GitComplex.Assembly CXX)

include(Molurus.Module)

add_molurus_library(${PROJECT_NAME} 17)
molurus_module_sources(PRIVATE Include Source)

target_link_libraries(${MODULE_TARGET}
  PUBLIC GitComplex.Presentation)

molurus_module_propagate_pic()
